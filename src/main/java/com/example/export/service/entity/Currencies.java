package com.example.export.service.entity;

import jakarta.persistence.*;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

@Entity
@Table(name = "currencies")
public class Currencies {
    @Id
    private Integer id;
    private String code;
    private String name;
    private Integer status;

    //getter and setter
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}

package com.example.export.service.repo;

import com.example.export.service.entity.Currencies;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CurrenciesRepository extends JpaRepository<Currencies, Integer> {
    @Query(value = "SELECT code, name, status" +
            "FROM Currencies " +
            "INTO OUTFILE 'C:\\Users\\yijin\\Documents\\currencies_backup.csv' " +
            "FIELDS ENCLOSED BY '\"' " +
            "TERMINATED BY ';' " +
            "ESCAPED BY '\"' " +
            "LINES TERMINATED BY '\\r\\n'", nativeQuery = true)
    void dumpCurrenciesDataToCsv();
}

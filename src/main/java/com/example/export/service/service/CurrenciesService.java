package com.example.export.service.service;

import au.com.bytecode.opencsv.CSVWriter;
import com.example.export.service.entity.Currencies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

@Service
@Transactional
public class CurrenciesService {

    @PersistenceContext
    private EntityManager entityManager;

    public void exportCurrenciesToCsv(String csvFilePath) throws IOException {
        List<Object[]> result = entityManager.createNativeQuery("SELECT code, name, status FROM currencies").getResultList();

        try (CSVWriter writer = new CSVWriter(new FileWriter(csvFilePath))) {
            writer.writeNext("Code", "Name", "Status");
            for (Object[] row : result) {
                writer.writeNext(
                        row[0].toString(),
                        row[1].toString(),
                        row[2].toString());
            }
        }
    }
}

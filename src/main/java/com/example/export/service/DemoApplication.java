package com.example.export.service;

import com.example.export.service.repo.CurrenciesRepository;
import com.example.export.service.service.CurrenciesService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.io.File;
import java.io.IOException;

@SpringBootApplication
@EntityScan("com.example.export.service.*")
@EnableJpaRepositories("com.example.export.service.repo")
@ComponentScan(basePackages = "com.example.export.service.*")
public class DemoApplication {

//	public static void main(String[] args) {
//		ApplicationContext context = SpringApplication.run(DemoApplication.class, args);
////		CurrenciesService currencyService = context.getBean(CurrenciesService.class);
//		CurrenciesRepository currenciesRepository = context.getBean(CurrenciesRepository.class);
//
//		//			currencyService.exportCurrenciesToCsv("C:\\Users\\yijin\\Documents\\currencies_backup.csv");
//		currenciesRepository.dumpCurrenciesDataToCsv();
//		System.out.println("Data dump completed successfully.");
//	}

	public static void main(String[] args) {
		try {
			dumpDataToCsv();
			System.out.println("Data dump completed successfully.");
		} catch (IOException | InterruptedException e) {
			System.out.println("Error occurred during data dump: " + e.getMessage());
		}
	}

	public static void dumpDataToCsv() throws IOException, InterruptedException {
		String mysqlPath = "C:\\Program Files\\MySQL\\MySQL Server 8.0\\bin\\";
		String command = "mysql -u root -pS3cret -h 127.0.0.1 -P 3309 game_aggregator_bo -e \"SELECT * FROM currencies\"";
		String outputFile = "C:\\Users\\yijin\\Documents\\currencies_backup.csv";

		ProcessBuilder processBuilder = new ProcessBuilder("cmd.exe", "/c", command);
		processBuilder.directory(new File(mysqlPath));
		processBuilder.redirectOutput(new File(outputFile));
		processBuilder.redirectErrorStream(true);

		Process process = processBuilder.start();
		int exitCode = process.waitFor();

		if (exitCode != 0) {
			throw new IOException("Failed to execute command: " + command);
		}

		System.out.println("CSV file generated successfully");
	}




}
